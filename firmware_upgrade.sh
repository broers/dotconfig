#!/bin/bash

set +eu

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

service fwupd start

sudo fwupdmgr refresh
sudo fwupdmgr update
